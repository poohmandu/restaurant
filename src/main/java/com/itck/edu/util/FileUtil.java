package com.itck.edu.util;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class FileUtil {

    public static String upload(MultipartFile uploadFile, HttpSession session, String fileUploadPath) throws IOException {

        // 重新为上传的文件生成 新的文件名称 UUID
        String fileName = uploadFile.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        String uuid = UUID.randomUUID().toString().toUpperCase().replace("-", "");
        fileName = uuid + suffix;

        // 获取真实路径
        // parent
        String parent = session.getServletContext().getRealPath(fileUploadPath);

        File file = new File(parent, fileName);
        if (!file.exists()) {
            file.mkdirs();
        }
        // 文件上传
        uploadFile.transferTo(file);

        // /upload/1.jpeg
        // 返回图片的路径
        return fileUploadPath + "/" + fileName;
    }
}