package com.itck.edu.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itck.edu.entity.Food;
import com.itck.edu.mapper.FoodMapper;
import com.itck.edu.model.Page;
import com.itck.edu.service.FoodMapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 *@ClassName FoodMapperServiceImpl
 *@Description TODO
 *@Author KevinYuk
 *@Date 2022/9/15 上午 11:31
 *@Version 1.0
 **/

@Service
public class FoodMapperServiceImpl implements FoodMapperService {

    @Autowired
    private FoodMapper foodMapper;

    @Override
    public List<Food> queryAll() {
        List<Food> list = new ArrayList<>();
        try {
            list = foodMapper.selectAll();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
        return list;
    }

    @Override
    public List<Food> queryByCategory(Integer category) {
        if(category == null) {
            throw new RuntimeException("未获得擦汗寻条件信息");
        }
        List<Food> list = new ArrayList<>();
        try {
            list = foodMapper.selectByCategory(category);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
        return list;
    }

    @Override
    public void addInfo(Food food) {
        if(food == null) {
            throw new RuntimeException("添加信息有误");
        }
        try {
            foodMapper.insertInfo(food);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Food queryById(Integer id) {
        if(id == null) {
            throw new RuntimeException("查询条件不存在");
        }
        Food food = null;
        try {
            food = foodMapper.selectById(id);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return food;
    }

    @Override
    public void removeById(Integer id) {
        if(id == null) {
            throw new RuntimeException("id不存在，无法删除指定数据");
        }
        try {
            foodMapper.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void updateInformation(Food food) {
        if(food == null) {
            throw new RuntimeException("更新信息异常");
        }
        try {
            foodMapper.updateInfo(food);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public Page pageQuery(Food food, Integer pageNumber,Integer pageSize) {
        //  使用分页插件对结果进行拦截
        PageHelper.startPage(pageNumber,pageSize);// 后面紧跟执行sql的代码
        PageInfo pageInfo = new PageInfo(foodMapper.query(food));
        return Page.createPage(pageInfo);
    }

}
