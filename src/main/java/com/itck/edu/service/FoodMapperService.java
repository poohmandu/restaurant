package com.itck.edu.service;

import com.itck.edu.entity.Food;
import com.itck.edu.model.Page;

import java.util.List;

public interface FoodMapperService {

    List<Food> queryAll();

    List<Food> queryByCategory(Integer category);

    void addInfo(Food food);

    Food queryById(Integer id);

    void removeById(Integer id);

    void updateInformation(Food food);

    Page pageQuery(Food food, Integer pageNumber, Integer pageSize);
}
