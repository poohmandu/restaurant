package com.itck.edu.controller;

import com.itck.edu.entity.Food;
import com.itck.edu.model.Page;
import com.itck.edu.model.Result;
import com.itck.edu.service.FoodMapperService;
import com.itck.edu.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 *@ClassName FoodController
 *@Description TODO
 *@Author KevinYuk
 *@Date 2022/9/15 上午 11:29
 *@Version 1.0
 **/

@RestController
@RequestMapping("/food")
public class FoodController {

    @Autowired
    private FoodMapperService foodMapperService;

    @Value("${file.upload.path}")
    private String fileUploadPath;

    @GetMapping("/list")
    public Page paginQuery(Food food,
                           @RequestParam(name = "pageNumber",defaultValue = "1")Integer pageNumber,
                           @RequestParam(name = "pageSize",defaultValue = "2")Integer pageSize) {
        return foodMapperService.pageQuery(food,pageNumber,pageSize);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public Map<String,Object> del(@PathVariable Integer id) {
        foodMapperService.removeById(id);
        Map<String,Object> result = new HashMap<>();
        result.put("code",200);
        result.put("msg","删除成功");
        return result;
    }

    @PostMapping("/add")
    public Result add(@RequestBody Food food) {
        food.setUpdateDate(new Date());
        foodMapperService.addInfo(food);
        return Result.ok("新增菜品成功");
    }

    @GetMapping("/{id}")
    public Result update(@PathVariable Integer id) {
        Food food = foodMapperService.queryById(id);
        return Result.ok("获取数据成功",food);
    }

    @PostMapping("/upload")
    public Result upload(MultipartFile uploadFile, HttpSession session) {
        try {
            String uri = FileUtil.upload(uploadFile, session, fileUploadPath);
            return Result.ok("文件上传成功", uri);
        } catch (IOException e) {
            e.printStackTrace();
            return Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
    }

    @PostMapping("/uploadForEditor")
    public Map<String, Object> uploadForEditor(MultipartFile uploadFile, HttpSession session)
            throws IOException {

        String uri = FileUtil.upload(uploadFile, session, fileUploadPath);
        Map<String, Object> result = new HashMap<>();
        result.put("errno", 0);

        Map<String, Object> imgMap = new HashMap<>();
        imgMap.put("url", "http://127.0.0.1:8080/restaurant" + uri);

        List list = new ArrayList();
        list.add(imgMap);

        result.put("data", list);
        return result;
    }

}
