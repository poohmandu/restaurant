package com.itck.edu.mapper;

import com.itck.edu.entity.Food;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;
import java.util.List;

/**
 *@ClassName FoodMapper
 *@Description TODO
 *@Author KevinYuk
 *@Date 2022/9/15 上午 09:55
 *@Version 1.0
 **/

public interface FoodMapper {

    /**
     * @Author KevinYuk
     * @Description 查询所有菜品信息
     * @Date 上午 10:20 2022/9/15
     * @Param []
     * @return java.util.List<com.itck.edu.entity.Food>
     **/
    List<Food> selectAll();

    /**
     * @Author KevinYuk
     * @Description 根据菜品的类型查询
     * @Date 上午 10:21 2022/9/15
     * @Param [category]
     * @return java.util.List<com.itck.edu.entity.Food>
     **/
    List<Food> selectByCategory(@Param("category") Integer category);

    /**
     * @Author KevinYuk
     * @Description 添加新的菜品信息
     * @Date 上午 10:59 2022/9/15
     * @Param [food]
     * @return void
     **/
    void insertInfo(@Param("food") Food food);

    /**
     * @Author KevinYuk
     * @Description 执行修改操作时，用来绑定数据
     * @Date 上午 11:03 2022/9/15
     * @Param
     * @return
     **/
    Food selectById(@Param("id") Integer id);

    /**
     * @Author KevinYuk
     * @Description 根据id删除信息
     * @Date 上午 11:17 2022/9/15
     * @Param []
     * @return void
     **/
    void deleteById(@Param("id") Integer id);

    /**
     * @Author KevinYuk
     * @Description 更新菜品
     * @Date 上午 11:23 2022/9/15
     * @Param [food, id]
     * @return void
     **/
    void updateInfo(@Param("food") Food food);

    List<Food> query(Food food);

//    void addMy(Food food);
}
