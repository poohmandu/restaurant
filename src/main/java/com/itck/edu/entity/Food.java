package com.itck.edu.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 *@ClassName Food
 *@Description
 *@Author KevinYuk
 *@Date 2022/9/15 上午 10:13
 *@Version 1.0
 **/
public class Food {
    private Integer id;

    private String foodName;

    // 菜品类型
    private Integer category;

    private String img;

    private Double price;

    private Double discount;

    // 简介
    private String synopsis;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateDate;

    public Food() {
    }

    public Food(String foodName, Integer category, String img, Double price, Double discount, String synopsis, Date updateDate) {
        this.foodName = foodName;
        this.category = category;
        this.img = img;
        this.price = price;
        this.discount = discount;
        this.synopsis = synopsis;
        this.updateDate = updateDate;
    }

    public Food(Integer id, String foodName, Integer category, String img, Double price, Double discount, String synopsis, Date updateDate) {
        this.id = id;
        this.foodName = foodName;
        this.category = category;
        this.img = img;
        this.price = price;
        this.discount = discount;
        this.synopsis = synopsis;
        this.updateDate = updateDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "FoodMapper{" +
                "id=" + id +
                ", foodName='" + foodName + '\'' +
                ", category=" + category +
                ", img='" + img + '\'' +
                ", price=" + price +
                ", discount=" + discount +
                ", synopsis='" + synopsis + '\'' +
                ", updateDate=" + updateDate +
                '}';
    }
}
