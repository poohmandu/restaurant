package com.itck.edu.model;

/**
 *@ClassName Result
 *@Description TODO
 *@Author KevinYuk
 *@Date 2022/9/9 上午 09:50
 *@Version 1.0
 **/
public class Result {
    private int code;// status code

    private String message;// result

    private Object object;

    /**
     * @Author KevinYuk
     * @Description 请求成功
     * @Date 上午 09:54 2022/9/9
     * @Param [msg]
     * @return com.example.springmvc.vo.Result
     **/
    public static Result ok(String msg) {
        Result result = new Result();
        result.setCode(200);//200表示请求成功
        result.setMessage(msg);
        return result;
    }

    public static Result ok(String msg,Object object) {
        Result result = new Result();
        result.setCode(200);//200表示请求成功
        result.setMessage(msg);
        result.setObject(object);
        return result;
    }

    /**
     * @Author KevinYuk
     * @Description 请求失败
     * @Date 上午 09:56 2022/9/9
     * @Param [code, msg]
     * @return com.example.springmvc.vo.Result
     **/
    public static Result error(int code,String msg) {
        Result result = new Result();
        result.setCode(code);
        result.setMessage(msg);
        return result;
    }

    public Result() {
    }

    public Result(int code, String message, Object object) {
        this.code = code;
        this.message = message;
        this.object = object;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", object=" + object +
                '}';
    }
}
