package com.itck.edu.model;

import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 *@ClassName Page
 *@Description TODO
 *@Author KevinYuk
 *@Date 2022/9/14 上午 11:09
 *@Version 1.0
 **/
public class Page {
//    total 满足查询条件的记录数
    private Long total;
//     rows  当前页的数据集合
    private List rows;

    public static Page createPage(PageInfo pageInfo) {
        return new Page(pageInfo.getTotal(),pageInfo.getList());
    }

    public Page() {
    }

    public Page(Long total, List rows) {
        this.total = total;
        this.rows = rows;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List getRows() {
        return rows;
    }

    public void setRows(List rows) {
        this.rows = rows;
    }
}
