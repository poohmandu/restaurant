package com.itck.common.controller;


import com.itck.edu.model.Result;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice // 对Controller层的异常处理进行增强
// @ControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(Throwable.class)
    // @ResponseBody
    public Result exceptionHandler(Throwable e) {
        return Result.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
    }
}
