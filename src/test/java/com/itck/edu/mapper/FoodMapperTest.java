package com.itck.edu.mapper;

import com.itck.edu.entity.Food;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-config.xml")
public class FoodMapperTest {

    @Autowired
    private FoodMapper foodMapper;

    @Test
    public void selectAll() {
        List<Food> foods = foodMapper.selectAll();
        for (Food food : foods) {
            System.out.println(food);
        }
    }

    @Test
    public void selectByCategory() {
        Integer category = 1;
        List<Food> foods = foodMapper.selectByCategory(1);
        for (Food food : foods) {
            System.out.println(food);
        }
    }

    @Test
    public void insertInfo() {
        Food food = new Food(6,"红烧胖大海",1,"sea.jpg",66.67,0.05,"海的味道我知道",new Date());
        foodMapper.insertInfo(food);
    }

    @Test
    public void selectById() {
        Integer id =  3;
        Food food = foodMapper.selectById(id);
        System.out.println(food);
    }

    @Test
    public void deleteById() {
        Integer id = 4;
        foodMapper.deleteById(id);
    }

    @Test
    public void updateInfo() {
        Food food = new Food(5,"爆缸菊花鸡",1,"chilly.jpg",99.99,0.15,"喷射战士zz",new Date());

        foodMapper.updateInfo(food);
    }
}